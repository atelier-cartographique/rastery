
from setuptools import setup, find_packages


name = 'rastery'
package = 'rastery'
description = 'A tiny WMS to help us build image processing pipelines.'
url = 'https://gitlab.com/atelier-cartographique/rastery'
author = 'Pierre Marchand'
author_email = 'pierremarc07@gmail.com'
license = 'Affero GPL3'
classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
    'Operating System :: POSIX',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.5',
]
install_requires = [
    "bottle",
    "pygdal",
    "python-slugify",
    "Pillow"
]
packages = ['rastery']
package_dir = {'rastery': 'rastery'}
package_data = {'rastery': ['templates/*']}
entry_points = {'console_scripts': ['rastery=rastery.wms:main']}

setup(
    name=name,
    version='0.1.0',
    url=url,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=packages,
    package_dir=package_dir,
    package_data=package_data,
    install_requires=install_requires,
    classifiers=classifiers,
    entry_points=entry_points
)
