# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
from base64 import b64decode
from osgeo import osr
from bottle import (
    request,
    response,
     route,
      run,
       abort,
        )
from jinja2 import Template
from pkg_resources import resource_string


import logging
logger = logging.getLogger('rastery')

from .lingua import LinguaWMS, BoundingBox
from .tile import create_tile_handler
from .config import Config

config = Config.instance()
config_tile = config.get('tile')
tile_handler = create_tile_handler(config_tile)

template_capabilities = Template(
    resource_string('rastery', 
        'templates/wms_capabilities.xml').decode('utf-8')
    )


ex_geo_srs = osr.SpatialReference()
ex_geo_srs.SetWellKnownGeogCS('CRS:84')

EPSG = dict()

def ex_geo(srs, bbox):
    if srs in EPSG:
        source_srs = EPSG.get(srs)
    else:
        code = int(srs.split(':')[1])
        source_srs = osr.SpatialReference()
        source_srs.ImportFromEPSG(code)
        EPSG[srs] = source_srs
    ct = osr.CreateCoordinateTransformation(source_srs, ex_geo_srs)
    minx, miny, z = ct.TransformPoint(bbox.minx, bbox.miny)
    maxx, maxy, z = ct.TransformPoint(bbox.maxx, bbox.maxy)
    return BoundingBox(minx, miny, maxx, maxy)


cap_req_parms_m = [
    LinguaWMS.Request.service,
    # LinguaWMS.Request.request
]
cap_req_parms_o = [
    LinguaWMS.Request.version,
    LinguaWMS.Request.format,
    LinguaWMS.Request.updatesequence
]
map_req_parms_m = [
    LinguaWMS.Request.version,
    # LinguaWMS.Request.request,
    LinguaWMS.Request.layers,
    LinguaWMS.Request.styles,
    LinguaWMS.Request.crs,
    LinguaWMS.Request.bbox,
    LinguaWMS.Request.width,
    LinguaWMS.Request.height
]
map_req_parms_o = [
    LinguaWMS.Request.transparent,
    LinguaWMS.Request.bgcolor,
    LinguaWMS.Request.exceptions,
    LinguaWMS.Request.time,
    LinguaWMS.Request.elevation
]


def get_capabilities():
    query = request.query
    params = LinguaWMS.Request.gets(query, cap_req_parms_m)
    params.update(
        LinguaWMS.Request.gets(query, cap_req_parms_o, False)
    )

    surl = request.urlparts
    ctx = {
        'wms_url': '{}://{}{}'.format(surl.scheme, surl.netloc, surl.path),
        'version': config_tile.get('version'),
        'title': config_tile.get('title'),
        'mime': config_tile.get('mime')
    }

    layers = []

    for layer in config_tile.get('layers'):
        srs = layer.get('srs')
        bbox = BoundingBox._make(layer.get('bbox'))
        geo = ex_geo(srs, bbox)
        layers.append({
            'name': layer.get('name'),
            'title': layer.get('title'),
            'srs': srs,
            'bbox': bbox,
            'geo': geo
        })
    ctx['layers'] = layers

    logger.debug('WMS:GetCapabilities {}'.format(ctx))
    # logger.debug('WMS:GetCapabilities template {}'.format(template_capabilities))
    response.content_type = 'application/xml; charset=UTF8'

    body =  template_capabilities.render(ctx)
    # logger.debug('WMS:GetCapabilities type of body {}'.format(type(body)))
    return body


def get_map():
    query = request.query
    params = LinguaWMS.Request.gets(query, map_req_parms_m)
    params.update(
        LinguaWMS.Request.gets(query, map_req_parms_o, False)
    )
    response.set_header('Content-Type', config_tile.get('mime'))

    return tile_handler.get_tile(params)


@route('/')
def get():
    wms_req = LinguaWMS.Request.get(request.query,
                                    LinguaWMS.Request.request)

    if LinguaWMS.Token.GetCapabilities == wms_req:
        return get_capabilities()
    elif LinguaWMS.Token.GetMap == wms_req:
        return get_map()

    abort(400, 'Not A Valid Request')




def main():
    logging.basicConfig(level=logging.DEBUG)
    host = config.get('http.host', 'localhost')
    port = config.get('http.port', 8000)
    debug = config.get('http.debug', False)
    server = config.get('http.server', 'auto')

    logger.info('Start server on {}:{} with server = {}'.format(
        host, port, server))

    run(server=server, host=host, port=port, debug=True)


if __name__ == '__main__':
    main()
