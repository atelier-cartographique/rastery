# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
from importlib import import_module
from osgeo import gdal, osr
gdal.UseExceptions()
import numpy
from PIL import Image
from io import BytesIO
from collections import namedtuple

Intersection = namedtuple('Intersection', ['x', 'y', 'w', 'h'])

import logging
logger = logging.getLogger('tev')

from .base import world2Pixel, BaseLayer, round_bbox



def default_process_image(im):
    return im


def create_image(np_array):
    bn, w, h = np_array.shape
    bands = []
    for a in np_array:
        bands.append(Image.fromarray(a, mode='L'))
    return Image.merge('RGB', bands)

def intersection(rxs, rys, bbox):
    x = min(rxs, max(0, bbox[0]))
    y = min(rys, max(0, bbox[1]))
    w = min(rxs, bbox[2]) - x
    h = min(rys, bbox[3]) - y

    return Intersection(x, y, w, h)

def is_valid_intersection(i):
    return  (
        i.w > 0 and i.h > 0 
        )

class GdalLayer(BaseLayer):

    def __init__(self, name, options):
        self.name = name
        self.path = options.get('path')
        self.processor = default_process_image
        self.source = None

        pmodule_name = options.get('processor')
        if pmodule_name is not None:
            mod = import_module(pmodule_name)
            self.processor = getattr(mod, 'process_image')
            logger.debug('Imported processor from {}'.format(pmodule_name))

    def get_source(self):
        if not self.source:
            self.source = gdal.Open(self.path)
        return self.source

    def render(self, sz, bbox):
        # bbox = round_bbox(bbox_f)
        logger.debug('gdal:render {} {}'.format(sz, bbox))
        source = self.get_source()
        bands = numpy.zeros((3, sz.height, sz.width), dtype=numpy.uint8)
        sgt = source.GetGeoTransform()

        # world2pixel gives a line number for y, from top to bottom in the
        # image
        minx, maxy = world2Pixel(sgt, bbox.minx, bbox.miny)
        maxx, miny = world2Pixel(sgt, bbox.maxx, bbox.maxy)
        it = intersection(source.RasterXSize, source.RasterYSize,
                [minx, miny, maxx, maxy] )
        logger.debug('gdal:render:bbox [{}, {}, {}, {}] => {}'.format(
            minx, miny, maxx,  maxy, it
        ))

        target = Image.new('RGB', sz, (255 ,255, 255))
        
        if is_valid_intersection(it):
            source.ReadAsArray(
                it.x, it.y, it.w, it.h, bands,
                resample_alg=gdal.GRIORA_Lanczos
            )
            im = self.processor(create_image(bands))
            target.paste(im, (it.x - minx, it.y - miny,))

        f = BytesIO()
        target.save(f, 'PNG')
        
        return f.getvalue()


