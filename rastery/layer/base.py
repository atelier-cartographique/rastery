

class BaseLayer:

    def __init__(self, name, options):
        raise NotImplementedError()

    def render(self, sz, bbox):
        raise NotImplementedError()


def round_bbox(bbox):
    return BoundingBox(*[int(x) for x in bbox])


def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)
