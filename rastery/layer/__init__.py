
from .gdal import GdalLayer
# from .mapnik import MapnikLayer


MODULES = {
    'gdal': GdalLayer,
    # 'mapnik': MapnikLayer
}


def create_layer(name, source):
    options = source.get('options')
    layer_module = source.get('module')
    return MODULES[layer_module](name, options)
