# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import os
from pathlib import Path

import logging
logger = logging.getLogger('rastery')


class ConfigNoInstance(Exception):

    """
    After walking up the root, could not find a config file
    """

    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


def find_config_file(p, name):
    if p == p.parent:
        raise ConfigNoInstance(name + '.json')

    cf = p.joinpath(name + '.json')
    if cf.exists():
        return cf
    return find_config_file(p.parent, name)


def path_key(root_data, path, default_value):
    path = path.split('.')
    data = root_data
    for seg in path:
        try:
            data = data[seg]
        except Exception:
            return default_value
    return data


class Config:

    def __init__(self, data):
        self.data = data

    @staticmethod
    def instance(name='rastery'):
        config_file = find_config_file(Path().absolute(), name)
        with config_file.open() as f:
            data = json.load(f)
            return Config(data)

    def get(self, key, default=None):
        return path_key(self.data, key, default)

    def get_config(self, key, default=dict()):
        return Config(self.get(key, default))

    def get_json(self):
        return json.dumps(self.data, indent=4)
